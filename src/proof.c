#ifndef NPROOFS

#include "allocate.h"
#include "file.h"
#include "inline.h"

struct proof
{
  bool binary;
  file *file;
  ints line;
  uint64_t added;
  uint64_t deleted;
  uint64_t lines;
  uint64_t literals;
};

void
kissat_init_proof (kissat * solver, file * file, bool binary)
{
  assert (file);
  assert (!solver->proof);
  proof *proof = kissat_calloc (solver, 1, sizeof (struct proof));
  proof->binary = binary;
  proof->file = file;
  solver->proof = proof;
  LOG ("starting to trace %s proof", binary ? "binary" : "non-binary");

  if (!binary) {
    kissat_put_string(proof->file, "pseudo-Boolean proof version 1.2\nf\n");
  }

}

void
kissat_release_proof (kissat * solver)
{
  proof *proof = solver->proof;
  assert (proof);
  LOG ("stopping to trace proof");
  RELEASE_STACK (proof->line);
  kissat_free (solver, proof, sizeof (struct proof));
  solver->proof = 0;
}

#ifndef QUIET

#include <inttypes.h>

#define PERCENT_LINES(NAME) \
  kissat_percent (proof->NAME, proof->lines)

void
kissat_print_proof_statistics (kissat * solver, bool verbose)
{
  proof *proof = solver->proof;
  PRINT_STAT ("proof_added", proof->added,
	      PERCENT_LINES (added), "%", "per line");
  PRINT_STAT ("proof_bytes", proof->file->bytes,
	      proof->file->bytes / (double) (1 << 20), "MB", "");
  PRINT_STAT ("proof_deleted", proof->deleted,
	      PERCENT_LINES (deleted), "%", "per line");
  if (verbose)
    PRINT_STAT ("proof_lines", proof->lines, 100, "%", "");
  if (verbose)
    PRINT_STAT ("proof_literals", proof->literals,
		kissat_average (proof->literals, proof->lines),
		"", "per line");
}

#endif

static void
import_internal_proof_literal (kissat * solver, proof * proof, unsigned ilit)
{
  int elit = kissat_export_literal (solver, ilit);
  assert (elit);
  PUSH_STACK (proof->line, elit);
  proof->literals++;
}

static void
import_external_proof_literal (kissat * solver, proof * proof, int elit)
{
  assert (elit);
  PUSH_STACK (proof->line, elit);
  proof->literals++;
}

static void
import_internal_proof_binary (kissat * solver, proof * proof,
			      unsigned a, unsigned b)
{
  assert (EMPTY_STACK (proof->line));
  import_internal_proof_literal (solver, proof, a);
  import_internal_proof_literal (solver, proof, b);
}

static void
import_internal_proof_literals (kissat * solver, proof * proof,
				size_t size, unsigned *ilits)
{
  assert (EMPTY_STACK (proof->line));
  assert (size <= UINT_MAX);
  for (size_t i = 0; i < size; i++)
    import_internal_proof_literal (solver, proof, ilits[i]);
}

static void
import_external_proof_literals (kissat * solver, proof * proof,
				size_t size, int *elits)
{
  assert (EMPTY_STACK (proof->line));
  assert (size <= UINT_MAX);
  for (size_t i = 0; i < size; i++)
    import_external_proof_literal (solver, proof, elits[i]);
}

static void
import_proof_clause (kissat * solver, proof * proof, clause * c)
{
  import_internal_proof_literals (solver, proof, c->size, c->lits);
}

static void
print_binary_proof_line (proof * proof)
{
  assert (proof->binary);
  for (all_stack (int, elit, proof->line))
    {
      unsigned x = 2u * ABS (elit) + (elit < 0);
      unsigned char ch;
      while (x & ~0x7f)
	{
	  ch = (x & 0x7f) | 0x80;
	  kissat_putc (proof->file, ch);
	  x >>= 7;
	}
      kissat_putc (proof->file, (unsigned char) x);
    }
  kissat_putc (proof->file, 0);
}

static void
print_non_binary_proof_line (proof * proof)
{
  assert (!proof->binary);
  char buffer[16];
  char *end_of_buffer = buffer + sizeof buffer;
  *--end_of_buffer = 0;
  int pivotLit = 0;
  for (all_stack (int, elit, proof->line))
    {
      if (pivotLit == 0) {
        pivotLit = elit;
      }
      kissat_putc (proof->file, '1');
      kissat_putc (proof->file, ' ');
      char *p = end_of_buffer;
      assert (!*p);
      assert (elit);
      assert (elit != INT_MIN);
      unsigned eidx;
      if (elit < 0)
        {
          kissat_putc (proof->file, '~');
          eidx = -elit;
        }
      else
        eidx = elit;
      kissat_putc (proof->file, 'x');
      for (unsigned tmp = eidx; tmp; tmp /= 10)
        *--p = '0' + (tmp % 10);
      while (p != end_of_buffer)
        kissat_putc (proof->file, *p++);
      kissat_putc (proof->file, ' ');
    }
  kissat_put_string (proof->file, ">= 1 ;");
}

static void
print_proof_line (proof * proof)
{
  proof->lines++;
  if (proof->binary)
    print_binary_proof_line (proof);
  else
    print_non_binary_proof_line (proof);
  CLEAR_STACK (proof->line);
#ifndef NDEBUG
  fflush (proof->file->file);
#endif
}

static void
print_added_proof_line (proof * proof)
{
  proof->added++;
  if (proof->binary)
    kissat_putc (proof->file, 'a');
  else {
    kissat_put_string (proof->file, "red ");
  }

  int pivotLit = 0;
  if (!EMPTY_STACK(proof->line)) {
    pivotLit = PEEK_STACK(proof->line, 0);
  }

  print_proof_line (proof);
  if (!proof->binary) {
    if (pivotLit) {
      char buffer[16];
      char *end_of_buffer = buffer + sizeof buffer;
      *--end_of_buffer = 0;

      char value = '1';
      if (pivotLit < 0) {
        value = '0';
        pivotLit = -pivotLit;
      }

      kissat_putc (proof->file, ' ');
      kissat_putc (proof->file, 'x');
      char *p = end_of_buffer;
      for (unsigned tmp = pivotLit; tmp; tmp /= 10)
        *--p = '0' + (tmp % 10);
      while (p != end_of_buffer)
        kissat_putc (proof->file, *p++);
      kissat_putc (proof->file, ' ');
      kissat_putc (proof->file, value);
    }

    kissat_putc (proof->file, '\n');

    if (!pivotLit) {
      // empty clause declare contradiction
      kissat_put_string (proof->file, "c -1 \n");
    }
  }
}

static void
print_delete_proof_line (proof * proof)
{
  proof->deleted++;
  if (proof->binary){
    kissat_putc (proof->file, 'd');
  } else {
    kissat_put_string (proof->file, "del find ");
  }
  print_proof_line (proof);
  if (!proof->binary)
    kissat_putc (proof->file, '\n');
}

void
kissat_add_binary_to_proof (kissat * solver, unsigned a, unsigned b)
{
  proof *proof = solver->proof;
  assert (proof);
  import_internal_proof_binary (solver, proof, a, b);
  print_added_proof_line (proof);
}

void
kissat_add_clause_to_proof (kissat * solver, clause * c)
{
  proof *proof = solver->proof;
  assert (proof);
  import_proof_clause (solver, proof, c);
  print_added_proof_line (proof);
}

void
kissat_add_empty_to_proof (kissat * solver)
{
  proof *proof = solver->proof;
  assert (proof);
  assert (EMPTY_STACK (proof->line));
  print_added_proof_line (proof);
}

void
kissat_add_lits_to_proof (kissat * solver, size_t size, unsigned *ilits)
{
  proof *proof = solver->proof;
  assert (proof);
  import_internal_proof_literals (solver, proof, size, ilits);
  print_added_proof_line (proof);
}

void
kissat_add_unit_to_proof (kissat * solver, unsigned ilit)
{
  proof *proof = solver->proof;
  assert (proof);
  assert (EMPTY_STACK (proof->line));
  import_internal_proof_literal (solver, proof, ilit);
  print_added_proof_line (proof);

  kissat_put_string(proof->file, "core id -1\n");
}

void
kissat_shrink_clause_in_proof (kissat * solver, clause * c,
			       unsigned remove, unsigned keep)
{
  proof *proof = solver->proof;
  const value *values = solver->values;
  assert (EMPTY_STACK (proof->line));
  for (all_literals_in_clause (ilit, c))
    {
      if (ilit == remove)
	continue;
      if (ilit != keep && values[ilit] < 0 && !LEVEL (ilit))
	continue;
      import_internal_proof_literal (solver, proof, ilit);
    }
  print_added_proof_line (proof);
  import_proof_clause (solver, proof, c);
  print_delete_proof_line (proof);
}

void
kissat_delete_binary_from_proof (kissat * solver, unsigned a, unsigned b)
{
  proof *proof = solver->proof;
  assert (proof);
  import_internal_proof_binary (solver, proof, a, b);
  print_delete_proof_line (proof);
}

void
kissat_delete_clause_from_proof (kissat * solver, clause * c)
{
  proof *proof = solver->proof;
  assert (proof);
  import_proof_clause (solver, proof, c);
  print_delete_proof_line (proof);
}

void
kissat_delete_external_from_proof (kissat * solver, size_t size, int *elits)
{
  proof *proof = solver->proof;
  assert (proof);
  import_external_proof_literals (solver, proof, size, elits);
  print_delete_proof_line (proof);
}

void
kissat_delete_internal_from_proof (kissat * solver,
				   size_t size, unsigned *ilits)
{
  proof *proof = solver->proof;
  assert (proof);
  import_internal_proof_literals (solver, proof, size, ilits);
  print_delete_proof_line (proof);
}

void
kissat_print_solution_to_proof (kissat * solver, int max_var)
{
  proof *proof = solver->proof;
  if (!proof->binary) {
    kissat_putc (proof->file, 'o');
    kissat_putc (proof->file, 'v');
    for (int eidx = 1; eidx <= max_var; eidx++)
    {
      int lit = kissat_value (solver, eidx);
      if (!lit)
        lit = eidx;

      char buffer[16];
      char *end_of_buffer = buffer + sizeof buffer;
      *--end_of_buffer = 0;

      kissat_putc (proof->file, ' ');
      if (lit < 0) {
        kissat_putc (proof->file, '~');
        lit = -lit;
      }


      kissat_putc (proof->file, 'x');
      char *p = end_of_buffer;
      for (unsigned tmp = lit; tmp; tmp /= 10)
        *--p = '0' + (tmp % 10);
      while (p != end_of_buffer)
        kissat_putc (proof->file, *p++);
    }
    kissat_putc (proof->file, '\n');
  }
}

#else
int kissat_proof_dummy_to_avoid_warning;
#endif
